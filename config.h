//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/				/*Update Interval*/	/*Update Signal*/
	{"",		"sb-portage",				300,			11},
	// {"",		"sb-crypto xmr-btc \"XMR-BTC\" 󰦝 25",	9000,			23},
	// {"",		"sb-crypto xmr Monero 󰦝 24",		9000,			22},
	// {"",		"sb-crypto btc-usd Bitcoin ",		300,			20},
	{"",		"sb-memory",				10,			9},
	{"",		"sb-loadavg",				10,			7},
	{"",		"sb-nettraf",				1,			5},
	{"",		"sb-volume",				0,			3},
	{"",		"sb-dtg",				60,			1}
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = " | ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

